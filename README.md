# LeetCode Problem Data Analysis

## Overview
This project analyzes a dataset of problems submitted by various companies and provides insights into the acceptance rate of these problems. It also explores the relationship between topics and companies and how they affect the acceptance rate of the problems.

The project uses Python and several data analysis libraries such as Pandas and Matplotlib to read and process the data, create visualizations, and extract insights.

## Features
- Calculates the mean acceptance rate of problems by company
- Determines the most common topics for each company
- Plots the distribution of problem acceptance rates by rating
- Identifies outliers in the acceptance rate data